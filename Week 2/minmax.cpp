#include <iostream>

using namespace std;


//uses if/elif to compare min/max
void compare(int x){
   int min = 0, max = 0, currentInt;
   cout << "Please enter " << x << " integers." << endl;

   for(int i = 0; i < x; i++){
      cin >> currentInt;
      if (currentInt <= min)
         min = currentInt;
      else if(currentInt >= max)
         max = currentInt;
   }

   cout << "min: " << min << endl;
   cout << "max: " << max << endl;

   return;
}


// prompts the user and then calls the comparison function
int main() {
   int count;

   cout << "How many integers would you like to enter?" << endl;
   cin >> count;
   compare(count);
   return 0;
}
