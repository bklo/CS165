/*********************************************************************
** Author: Brandon Lo
** Date:10/2/2016
** Description: Program that calculates the distance fallen over a given time period
*********************************************************************/

#include <iostream>
#include <cmath>
using namespace std;

//returns a double value after inputting time and returning the distance
double fallDistance(double t){
   const double G = 9.8;
   double d;

   d = 0.5 * G * pow(t,2.0);

   return d;
}

/*int main() {
   double falling, testReturn;

   cout << "Time fallen: ";
   cin >> falling;
   testReturn = fallDistance(falling);
   cout << testReturn << endl;

   return 0;
}*/
