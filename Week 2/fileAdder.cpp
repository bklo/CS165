/*********************************************************************
** Author: Brandon Lo
** Date:10/2/2016
** Description: Program that reads a text file
*********************************************************************/

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

//checks if filename can be opened
bool check(string fileName){
   ifstream inputFile;

   inputFile.open(fileName);
   if(!inputFile)
      return false;
   else
      return true;
}


int main() {
   ifstream inputFile;
   ofstream outputFile;
   int number, sum = 0;
   string fileName;
   bool fileStatus;

   cout << "Enter filename: ";
   cin >> fileName;
   fileStatus = check(fileName);
   if (!fileStatus)
      cout << "Could not access file." << endl;
   else{
      inputFile.open(fileName);
      outputFile.open("sum.txt");
      while (inputFile >> number){
         sum = sum + number;
      }
      outputFile << sum;
      fileStatus = check("sum.txt");
      if (!fileStatus)
         cout << "Failed to write file." << endl;
      else{
         inputFile.close();
         outputFile.close();
      }
   }

   return 0;
}
