/*********************************************************************
** Author: Brandon Lo
** Date:10/2/2016
** Description: Number guessing program
*********************************************************************/
#include <iostream>
using namespace std;


//a do while loop that has a counter and a check for the guess
int main() {
   int guess, check, count = 1;

   cout << "Enter the number for the player to guess." << endl;
   cin >> guess;
   cout << "Enter your guess." << endl;

   do{
      cin >> check;
      if (check == guess)
         break;
      else if (check < guess)
         cout << "Too low - try again." << endl;
      else
         cout << "Too high - try again." << endl;
      count++;
   }while (guess != check);

   cout << "You guessed it in " << count << " tries." << endl;

   return 0;
}
