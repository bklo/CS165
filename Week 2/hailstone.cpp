/*********************************************************************
** Author: Brandon Lo
** Date:10/2/2016
** Description: Program that uses the hailstone sequence to give a count of how many steps it takes to reach 1.
*********************************************************************/

#include <iostream>
using namespace std;

//intial if else to detect if the exception occurs, else a while loop that continues until startingInt is 1. return startingInt as count.
int hailstone (int &startingInt){
   int count = 0;

   if (startingInt ==  1){
      startingInt = 0;
      return startingInt;
   }

   else{
      while (startingInt != 1){
         if (startingInt == 0)
            return 0;
         else if (startingInt%2 == 0){
            startingInt = startingInt/2;
            count++;
         }
         else if (startingInt%2 != 0){
            startingInt = startingInt * 3 + 1;
            count++;
         }
      }
   }

   startingInt = count;
   return startingInt;
}

/*
int main() {
   int a = 7;
   hailstone(a);
   cout << a << endl;
   a = 1;
   hailstone(a);
   cout << a << endl;
   a = 3;
   hailstone(a);
   cout << a << endl;

   return 0;
}
*/
