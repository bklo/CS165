/*********************************************************************
** Author: Brandon Lo
** Date:10/5/2016
** Description: Point class source file that takes in and sets an X and Y coordinate. Also utalizes pythagorean theorm to calculate distance between 2 points
*********************************************************************/
#include <iostream>
#include <cmath>
#include "Point.hpp"

//default constructor initializes to 0 for X and Y coordinates
Point::Point(){
   setXCoord(0);
   setYCoord(0);
}

//constructor sets the X and Y coordinates to user input
Point::Point(double inputX, double inputY){
   setXCoord(inputX);
   setYCoord(inputY);
}

//sets inputX vlaue to the private value
void Point::setXCoord(double inputX){
   xCoordinate = inputX;
}

//sets inputY value to the private value
void Point::setYCoord(double inputY){
   yCoordinate = inputY;
}

//returns xCoordinate, is const beacuse the point used in distanceTo requires const reference
double Point::getXCoord() const{
   return xCoordinate;
}

//returns yCoordinate is const beacuse the point used in distanceTo requires const reference
double Point::getYCoord() const{
   return yCoordinate;
}

//uses the pythagorean theroem to return distance
double Point::distanceTo(const Point &points){
   double deltaX,deltaY,distance;

   deltaX = (xCoordinate - points.getXCoord());
   deltaY = (yCoordinate - points.getYCoord());
   distance = sqrt(pow(deltaX,2.0) + pow(deltaY,2.0));

   return distance * 1.0;
}
