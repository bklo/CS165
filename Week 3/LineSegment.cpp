/*********************************************************************
** Author: Brandon Lo
** Date:10/5/2016
** Description: LineSegment class source file that utalizes the point specification file. Takes in and sets 2 points.
*********************************************************************/
#include <iostream>
#include <cmath>
#include "LineSegment.hpp"
#include "Point.hpp"

//constructor to take in 2 points and set them
LineSegment::LineSegment(Point inputPoint1, Point inputPoint2){
   setEnd1(inputPoint1);
   setEnd2(inputPoint2);
}

//sets the first point to the private variable
void LineSegment::setEnd1(Point inputPoint){
   endPoint1 = inputPoint;
}

//sets the second point to the private variable
void LineSegment::setEnd2(Point inputPoint){
   endPoint2 = inputPoint;
}

//returns the first point ("start" of the line point)
Point LineSegment::getEnd1(){
   return endPoint1;
}

//returns the second point("end" of the line point)
Point LineSegment::getEnd2(){
   return endPoint2;
}

//calculates and returns the slope from the slope formula slope = deltaY/deltaX
double LineSegment::slope(){
   double deltaX, deltaY, slope;

   deltaX = (endPoint2.getXCoord() - endPoint1.getXCoord());
   deltaY = (endPoint2.getYCoord() - endPoint1.getYCoord());
   slope = deltaY/deltaX;

   return slope;
}

//utalizes the point member function distanceTo to calulate the LineSegment length
double LineSegment::length(){
   return endPoint1.distanceTo(endPoint2);
}
