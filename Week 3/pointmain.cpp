

#include "Point.hpp"
#include "LineSegment.hpp"
#include <iostream>

int main()
{
  /*
  Point pt1(-2, 3.5),
   pt2(-1.5,3.5);
  LineSegment line1(pt1, pt2);
  std::cout << "Should see 0:  " << line1.slope() << std::endl ;
  */

  Point pt31(2.5, 2.6),
    pt32(-1.5,3.5);
  LineSegment line31(pt31, pt32);
  std::cout << "Should see -0.225:  " << line31.slope() << std::endl ;

  Point pt51(-1.5, 2.6),
    pt52(2.5,3.5),
    pt53(-5.8,4.3);
  LineSegment line51(pt51, pt52);
  line51.setEnd2(pt53);
  std::cout << "Should see -1.5:  " << line51.getEnd1().getXCoord() << std::endl ;
  std::cout << "Should see 2.6:  " << line51.getEnd1().getYCoord() << std::endl ;
  std::cout << "Should see -5.8:  " << line51.getEnd2().getXCoord() << std::endl ;
  std::cout << "Should see 4.3:  " << line51.getEnd2().getYCoord() << std::endl ;

  /*
  Point pt1(-1.5, 2.6),
    pt2(2.5,3.5),
    pt3(-5.8,4.3);
  LineSegment line1(pt1, pt2);
  line1.setEnd1(pt3);
  std::cout << "Should see -5.8:  " << line1.getEnd1().getXCoord() << std::endl ;
  std::cout << "Should see 4.3:  " << line1.getEnd1().getYCoord() << std::endl ;
  std::cout << "Should see 2.5:  " << line1.getEnd2().getXCoord() << std::endl ;
  std::cout << "Should see 3.5:  " << line1.getEnd2().getYCoord() << std::endl ;
  */

  /*
  Point pt1(-1.5, 2.6),
    pt2(2.5,3.5);
  LineSegment line1(pt1, pt2);
  std::cout << "Should see 4.1:  " << line1.length() << std::endl ;
  */

  /*
  Point pt1(-1.5, 2.6),
    pt2(2.5,3.5);
  std::cout << "Should see 4.1:  " << pt1.distanceTo(pt2) << std::endl ;
  */

  /*
  Point pt1(-10.5, 22.3);
  pt1.setYCoord(15.6);
  std::cout << "Should see -10.5:  " << pt1.getXCoord() << std::endl ;
  std::cout << "Should see 15.6:  " << pt1.getYCoord() << std::endl ;
  */

  /*
  Point pt1(-10.5, 22.3);
  pt1.setXCoord(15.6);
  std::cout << "Should see 15.6:  " << pt1.getXCoord() << std::endl ;
  std::cout << "Should see 22.3:  " << pt1.getYCoord() << std::endl ;
  */

  /*
  Point pt1(-2, 3.5),
    pt2(-1.5,3.5);
  LineSegment line1(pt1, pt2);
 std::cout << "Should see 0:  " << line1.slope() << std::endl ;
  */


  Point pt21(-1.5, 3.5),
    pt22(-2.0,3.5);
  LineSegment line21(pt21, pt22);
  std::cout << "Should see 0:  " << line21.slope() << std::endl ;

  Point p1(-1.5, 0.0);
  Point p2(1.5, 4.0);
  double dist = p1.distanceTo(p2);
  std::cout << "Should be 5.0  " << dist << std::endl;

  Point pt41(-2.0, 3.0),
    pt42(-2.0,3.5);
  LineSegment line41(pt41, pt42);
  std::cout << "Should see inf:  " << line41.slope() << std::endl ;

  /*
  Point p1(4.3, 7.52);
  Point p2(-17.0, 1.5);
  LineSegment ls1(p1, p2);
  double length = ls1.length();
  double slope = ls1.slope();
  std::cout << "Should be ??  " << slope << std::endl;
  std::cout << "Should be ??  " << length << std::endl;
  */
  return 0;


}
