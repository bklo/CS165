/*********************************************************************
** Author: Brandon Lo
** Date:10/5/2016
** Description: header file for Point class specification file
*********************************************************************/

#ifndef POINT_HPP
#define POINT_HPP

class Point{
   private:
      double xCoordinate,
      yCoordinate;
   public:
      Point();
      Point(double, double);
      double distanceTo(const Point&);
      void setXCoord(double);
      void setYCoord(double);
      double getXCoord() const;
      double getYCoord() const;
};

#endif
