/*********************************************************************
** Author: Brandon Lo
** Date:10/5/2016
** Description: header file for Box class specification file
*********************************************************************/
#ifndef BOX_HPP
#define BOX_HPP

//declaring class Box
class Box {
   private:
      double height,
      width,
      length;

   public:
      Box();
      Box(double, double, double);
      void setHeight(double);
      void setWidth(double);
      void setLength(double);
      double getVolume();
      double getSurfaceArea();
};

#endif
