/*********************************************************************
** Author: Brandon Lo
** Date:10/5/2016
** Description: header file for LineSegment class specification file, utalizes the Point class
*********************************************************************/
#ifndef LINESEGMENT_HPP
#define LINESEGMENT_HPP
#include "Point.hpp"

class LineSegment{
   private:
      Point endPoint1;
      Point endPoint2;
   public:
      LineSegment();
      LineSegment(Point, Point);
      void setEnd1(Point);
      void setEnd2(Point);
      Point getEnd1();
      Point getEnd2();
      double slope();
      double length();
};

#endif
