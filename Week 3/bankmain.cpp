#include "BankAccount.hpp"
#include <string>
#include <iostream>

int main()
{

  BankAccount account1("Corndogs", "pizza", 50);

  account1.deposit(100);

  account1.withdraw(10.5);

  std::cout << "Customer Balance is: (Should be 139.5): " << account1.getCustomerBalance() << std::endl;
  std::cout << "Customer Name is: (should be Corndogs) " << account1.getCustomerName() << std::endl;
  std::cout << "Customer ID is: (should be pizza) " << account1.getCustomerID() << std::endl;

  return 0;

}
