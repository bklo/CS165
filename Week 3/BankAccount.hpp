/*********************************************************************
** Author: Brandon Lo
** Date:10/5/2016
** Description: header file for BankAccount class specification file
*********************************************************************/
#ifndef BANKACCOUNT_HPP
#define BANKACCOUNT_HPP

#include <string>

class BankAccount{
   private:
      std::string customerName;
      std::string customerID;
      double customerBalance;
   public:
      BankAccount(std::string, std::string, double);
      std::string getCustomerName();
      std::string getCustomerID();
      double getCustomerBalance();
      void withdraw(double);
      void deposit(double);
};

#endif
