/*********************************************************************
** Author: Brandon Lo
** Date:10/5/2016
** Description: BankAccount class source file that  has a constructor, 3 get member functions, and 2 void functions to change the blaance
*********************************************************************/

#include "BankAccount.hpp"

//initializes the Bank Account and assigns to value
BankAccount::BankAccount(std::string inputName, std::string inputID, double inputBalance){
   customerName = inputName;
   customerID = inputID;
   customerBalance = inputBalance;
}

//returns customerName
std::string BankAccount::getCustomerName(){
   return customerName;
}

//returns customerID
std::string BankAccount::getCustomerID(){
   return customerID;
}

//returns customerBalance
double BankAccount::getCustomerBalance(){
   return customerBalance;
}

//Deaggregator for customerBalance
void BankAccount::withdraw(double inputWithdraw){
   customerBalance -= inputWithdraw;
}

//Aggregator for customerBalance
void BankAccount::deposit(double inputDeposit){
   customerBalance += inputDeposit;
}
