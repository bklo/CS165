#include "Box.hpp"
#include <iostream>

int main()
{

  Box box1;

  std::cout << "Volume is (should be 1): " << box1.getVolume() << std::endl;

  std::cout << "Surface Area is (should be 1): " << box1.getSurfaceArea() << std::endl;

  Box box2(3,5,8);

  std::cout << "Volume is (should be 120): " << box2.getVolume() << std::endl;

  std::cout << "Surface Area is (should be 158): " << box2.getSurfaceArea() << std::endl;

  double height,
    width,
    length;
  char choice;


  do
    {
      std::cout << "Set the Width" << std::endl;
      std::cin >> width;
      box1.setWidth(width);

      std::cout << "Set the Height" << std::endl;
      std::cin >> height;
      box1.setHeight(height);

      std::cout << "Set the Length" << std::endl;
      std::cin >> length;
      box1.setLength(length);


      //std::cout << "Width Is: " << box1.getWidth() << std::endl;
      //std::cout << "Length Is: " << box1.getLength() << std::endl;
      //std::cout << "Height Is: " << box1.getHeight() << std::endl;
      std::cout << "Surface Area is: " << box1.getSurfaceArea() << std::endl;
      std::cout << "Volume is: " << box1.getVolume() << std::endl;

      std::cout << "Exit? (y or n)" << std::endl;
      std::cin >> choice;

    } while (choice != 'y');

  return 0;

}
