/*********************************************************************
** Author: Brandon Lo
** Date:11/16/2016
** Description: Implementation of a CustomerProject class to set and get hours, materials, and transportation
*********************************************************************/

#include "CustomerProject.hpp"

//constructor to set the hours, materials, and transportation
CustomerProject::CustomerProject(double inputHours, double inputMaterials, double inputTrans){

  setHours(inputHours);
  setMaterials(inputMaterials);
  setTransportation(inputTrans);
}

//sets the hours
void CustomerProject::setHours(double inputHours){
  hours = inputHours;
}

//sets the materials
void CustomerProject::setMaterials(double inputMaterials){
  materials = inputMaterials;
}

//sets the transportation costs
void CustomerProject::setTransportation(double inputTrans){
  transportation = inputTrans;
}

//returns the hours
double CustomerProject::getHours(){
  return hours;
}

//returns the materials
double CustomerProject::getMaterials(){
  return materials;
}

//returns the transportation
double CustomerProject::getTransportation(){
  return transportation;
}
