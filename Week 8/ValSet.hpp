/*********************************************************************
** Author: Brandon Lo
** Date:11/16/2016
** Description: header file for ValSet class specification file
*********************************************************************/

#ifndef VALSET_HPP
#define VALSET_HPP

#include <vector>
using namespace std;

template <class T>
class ValSet{
   private:
      int arraySize,
      count;
      T *mainptr;
   public:
      ValSet();
      ValSet (ValSet<T> &);
      ValSet &operator=(const ValSet <T>&);
      ~ValSet();
      int size();
      bool isEmpty();
      bool contains(T);
      void add(T);
      void remove(T);
      vector<T> getAsVector();
};

#endif
