/*********************************************************************
** Author: Brandon Lo
** Date:11/16/2016
** Description: header file for RegularProject class specification file
*********************************************************************/

#ifndef REGULAR_PROJECT
#define REGULAR_PROJECT

#include "CustomerProject.hpp"

class RegularProject : public CustomerProject{

   private:

   public:
     RegularProject(double, double, double);
     double billAmount();

};


#endif
