/*********************************************************************
** Author: Brandon Lo
** Date:11/16/2016
** Description: Implementation of a RegularProject class to set and get hours, materials, and transportation
*********************************************************************/

#include "RegularProject.hpp"

RegularProject::RegularProject(double inputHours, double inputMaterials, double inputTrans)
  : CustomerProject(inputHours,inputMaterials,inputTrans)
{}

//overrides bill amount to return the sum of materials cost, transportation costs, and $80 times the number of hours
double RegularProject::billAmount(){
  double hours = CustomerProject::getHours();
  double materials = CustomerProject::getMaterials();
  double trans = CustomerProject::getTransportation();

  return (80*hours)+materials+trans;

}
