/*********************************************************************
** Author: Brandon Lo
** Date:11/16/2016
** Description: header file for PreferredProject class specification file
*********************************************************************/

#ifndef PREFERRED_PROJECT
#define PREFERRED_PROJECT

#include "CustomerProject.hpp"

class PreferredProject : public CustomerProject{

   private:

   public:
     PreferredProject(double, double, double);
     double billAmount();

};

#endif
