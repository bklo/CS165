/*********************************************************************
** Author: Brandon Lo
** Date:11/16/2016
** Description: Implementation of a template class to put values into a dynamic array
*********************************************************************/

#include <iostream>
#include <vector>
#include <string>
#include "ValSet.hpp"
using namespace std;


//initializes pointer to array size 10 and creates new array with size 10 and allocates memory for it. Also sets pointer to NULL
template <class T>
ValSet<T>::ValSet(){
   arraySize = 10;
   count = 0;
   mainptr = NULL;
   mainptr = new T[arraySize];

}

//copy constructor, copies over the array values over to the new instance
template <class T>
ValSet<T>::ValSet(ValSet<T> &object){

   arraySize = object.arraySize;
   count = object.count;
   mainptr = new T[arraySize];
   for (int i = 0; i < arraySize; i++)
      mainptr[i] = object.mainptr[i];

}


//operator overloadingm copies over the array and does a similar function to the copy constructor but instead returns *this
template <class T>
ValSet<T> &ValSet<T>::operator=(const ValSet<T> &object){

   while(this != &object){
      delete [] mainptr;
      arraySize = object.arraySize;
      mainptr = new T[arraySize];

      for (int i=0; i < arraySize; i++)
         mainptr[i] = object.mainptr[i];

   }

   return *this;
}



//deconstructor deletes the array and also resets the pointer to NULL
template <class T>
ValSet<T>::~ValSet(){
   delete[] mainptr;
   mainptr = NULL;
}


//returns size of what's inside the array. Based on count which is incremented or deincremented in the add/remove functions
template <class T>
int ValSet<T>::size(){
   return count;
};


//returns if multiPointer advances 1 or not
template <class T>
bool ValSet<T>::isEmpty(){
   if(count == 0)
      return true;
   else
      return false;
}

//checks to see if the array contains T, else it returns false
template <class T>
bool ValSet<T>::contains(T check){
   bool checkStatus = false;

   for (int i = 0; i < arraySize; i++){
      if(mainptr[i] == check)
         checkStatus = true;
   }

   return checkStatus;
}

//adds input into the dynamic array, called newInput, but is based off template class T.Affects the size() function too. uses the contains bool function to check if the input is added in or not
template <class T>
void ValSet<T>::add(T newInput){
   T * newptr;
   bool check = contains(newInput);

   if (check == false){
      if (count == arraySize){
         newptr = new T[arraySize];
         for (int i = 0; i < arraySize; i++)
            newptr[i] = mainptr[i];

         arraySize *= 2;
         for (int j = count; j < arraySize; j++)
            newptr[j] = T();

         delete[] mainptr;
         mainptr = newptr;
         add(newInput);
      }

      else{
         mainptr[count++] = newInput;
      }
   }
}

//removes item and deincrements the count and checks if contains
template <class T>
void ValSet<T>::remove(T removeItem){
   T *temp;

   if(!((*this).contains(removeItem))){
      temp = mainptr;
      for (int i = 0; i < arraySize; i++){
         if(temp[i] == removeItem){
            for (int j = i; j < arraySize - 1; j++){
               temp[j] = temp[j+1];
               }
         }
      }


    mainptr = temp;
    count--;
   }
}

//converts the dynamic array to a vector
template <class T>
vector<T> ValSet<T>::getAsVector(){
   vector<T> valueTown;
   for(int i =0; i < arraySize; i++){
      valueTown.push_back(mainptr[i]);
   }

   return valueTown;
}



int main()
{
   ValSet<char> mySet;
    mySet.add('A');
    mySet.add('A');
    mySet.add('A');
    mySet.add('A');
    mySet.add('A');
    mySet.add('A');
    mySet.add('j');
    mySet.add('&');
    mySet.remove('j');
    mySet.add('#');
    int size = mySet.size();
    ValSet<char> mySet2 = mySet;
    bool check1 = mySet2.contains('&');
    bool check2 = mySet2.contains('j');
    bool check3 = mySet2.contains('w');
    bool check4 = mySet2.isEmpty();
	vector<char> test = mySet.getAsVector();

	cout << "This is the size:   " << size << endl;
	if (check1 == true){
		cout << "Working check1" << endl;
	}
	if (check2 == false){
		cout << "Working check2" << endl;
	}

	if (check3 == false){
		cout << "Working check3" << endl;
	}

   if (check4 == false){
		cout << "Working check4" << endl;
	}

	for (int i = 0; i < size; i++)
		cout << test[i] << endl;
    return 0;
}



template class ValSet<int>;
template class ValSet<char>;
template class ValSet<std::string>;
