/*********************************************************************
** Author: Brandon Lo
** Date:11/16/2016
** Description: Implementation of a PreferredProject class to set and get hours, materials, and transportation
*********************************************************************/

#include "PreferredProject.hpp"


PreferredProject::PreferredProject(double inputHours, double inputMaterials, double inputTrans)
  : CustomerProject(inputHours,inputMaterials,inputTrans)
{}


//overrides bill amount to return the sum of 85% * materials cost, 90% * transportation costs, and $80 times the number of hours (max 100 hours)
double PreferredProject::billAmount(){
   double total = 0;
   double hours = CustomerProject::getHours();
   double materials = CustomerProject::getMaterials();
   double trans = CustomerProject::getTransportation();

   if (hours < 100){
    total = (80*hours)+(0.85 * materials)+ (0.9 * trans);
   }

   else if (hours >= 100){
      total = (80 * 100) + (0.85 * materials) + (0.9 * trans);
   }

   return total;
}
