/*********************************************************************
** Author: Brandon Lo
** Date:11/16/2016
** Description: header file for CustomerProject class specification file
*********************************************************************/

#ifndef CUSTOMER_PROJECT
#define CUSTOMER_PROJECT

class CustomerProject{

   protected:
     double hours,
       materials,
       transportation;

   public:
     void setHours(double);
     void setMaterials(double);
     void setTransportation(double);
     double getHours();
     double getMaterials();
     double getTransportation();
     CustomerProject(double, double, double);
     virtual double billAmount() = 0;

};

#endif
