/*********************************************************************
** Author: Brandon Lo
** Date:9/21/2016
** Description: A program converts Celsius to Fahrenheit
*********************************************************************/

#include <iostream>

// takes in celsius as input and multiplies by 9/5 and adds 32 to convert to F.

int main() {
  double celscius, fahrenheit;

  std::cout << "Please enter a Celsius temperature." << std::endl;
  std::cin >> celscius;
  fahrenheit = 9.0/5.0 * celscius + 32.0;
  std::cout << "The equivalent Fahrenheit temperature is: " << std::endl;
  std::cout << fahrenheit << std::endl;

  return 0;
}
