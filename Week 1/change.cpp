/*********************************************************************
** Author: Brandon Lo
** Date:9/21/2016
** Description: Implementation of the greedy coin algorithm
*********************************************************************/

#include <iostream>

//Greedy coin algorithm without loops or conditionals

int main() {
  unsigned int money, quarters, dimes, nickels, pennies, change;

  std::cout << "Please enter an amount in cents less than a dollar. " << std::endl;
  std::cin >> money;

  std::cout << "Your change will be:" << std::endl;

  quarters = money/25;
  std::cout << "Q: " << quarters << std::endl;
  change = money%25;

  dimes = change/10;
  std::cout << "D: "<< dimes << std::endl;
  change = change%10;

  nickels = change/5;
  std::cout << "N: "<< nickels << std::endl;
  change = change%5;

  pennies = change;
  std::cout <<"P: "<< pennies << std::endl;

  return 0;
}
