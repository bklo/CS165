
/*********************************************************************
** Author: Brandon Lo
** Date:9/21/2016
** Description: A program that takes 5 numbers and averages them
*********************************************************************/

#include <iostream>

//Takes in 5 numbers as variables then averages them

int main() {
  double a, b , c, d, e, sum, result;

  std::cout << "Please enter five numbers. " << std::endl;
  std::cin >> a >> b >> c >> d >> e;
  sum = a + b + c + d + e;
  result = sum/5.0;
  std::cout << "The average of those numbers is:" << std::endl;
  std::cout << result << std::endl;

  return 0;
}
