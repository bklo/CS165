/*********************************************************************
** Author: Brandon Lo
** Date:12/2/2016
** Description: Customer implementation file
*********************************************************************/
#include "Customer.hpp"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

//constructor for the customer which sets the public to the private variables
Customer::Customer(string n, string a, bool pm){
   name = n;
   accountID = a;
   premiumMember = pm;
}

//returns the accountID private variable
string Customer::getAccountID(){
   return accountID;
}

//returns the vector Cart private variable
vector<string> Customer::getCart(){
   return cart;
}

//adds a string item the vector Cart private variable
void Customer::addProductToCart(string item){
   cart.push_back(item);
}

//returns the bool if the customer is a premiumMember or not
bool Customer::isPremiumMember(){
   return premiumMember;
}

//clears the vector Cart
void Customer::emptyCart(){
   cart.clear();
}
