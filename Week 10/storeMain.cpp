#include "Store.hpp"
#include <iostream>
#include <string>
using namespace std;

int main() {

	Store myStore;
	Customer test("erik", "123", 0);
	Customer t2("erik", "500", true);
	Customer *ptrTest = &test;
	Customer *ptrt2 = &t2;
	Product widget("111", "widGet", "this is a test widget", 100.75, 3);
	Product *ptrWidget = &widget;
	std::string pID = "111";

	myStore.addProduct(ptrWidget);
	myStore.addMember(ptrTest);
	myStore.addMember(ptrt2);

	//myStore.productSearch("WIDGET");
	myStore.addProductToMemberCart(pID, "123");
	myStore.addProductToMemberCart(pID, "500");
	myStore.addProductToMemberCart(pID, "500");

	myStore.checkOutMember("500");

	cout<< endl;
	cout<< endl;
	myStore.checkOutMember("123");

	myStore.checkOutMember("500");

	return 0;
}
