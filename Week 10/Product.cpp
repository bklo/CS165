/*********************************************************************
** Author: Brandon Lo
** Date:12/2/2016
** Description: Product implementation file
*********************************************************************/
#include "Product.hpp"
#include <iostream>
#include <string>
using namespace std;

//constructor for the Product which sets the public to the private variables
Product::Product(string id, string t, string d, double p, int qa){
   idCode = id;
   title = t;
   description = d;
   price = p;
   quantityAvailable = qa;
}

//returns the idCode private variable
string Product::getIdCode(){
   return idCode;
}

//returns the title private variable
string Product::getTitle(){
   return title;
}

//returns the description private variable
string Product::getDescription(){
   return description;
}

//returns the price private variable
double Product::getPrice(){
   return price;
}

//returns the quantityAvailable private variable
int Product::getQuantityAvailable(){
   return quantityAvailable;
}

//decreases the quantityAvailable when called
void Product::decreaseQuantity(){
   quantityAvailable--;
}
