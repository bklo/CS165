/*********************************************************************
** Author: Brandon Lo
** Date:12/2/2016
** Description: header file for TicTacToe class specification file
*********************************************************************/
#ifndef TICTACTOE_HPP
#define TICTACTOE_HPP
#include "Board.hpp"

class TicTacToe{
   private:
      Board currentBoard;
      int playerTurnCount, OStatus, XStatus, option;
      char p1,
      p2,
      player;
   public:
      TicTacToe();
      TicTacToe(char player);
      void play();
      int turnCounter();
};

#endif
