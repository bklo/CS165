/*********************************************************************
** Author: Brandon Lo
** Date:12/2/2016
** Description: Board file which has the function for the board moves and the tictactoe game and the printing of the game status and the printing of the current gameboard
*********************************************************************/
#include "Board.hpp"
#include <iostream>
#include <string>
using namespace std;

//constructor that sets the entire 3x3 array to dots and sets the x and y coordinates to 0. Sets default playerturn to 1.
Board::Board(){
   x = 0;
   y = 0;
   playerTurn = 1;
   for (int i = 0; i < 3; i++){
      for (int q = 0; q < 3; ++q){
         board[i][q] = '.';
      }
   }
}

//makes move and changes the placement on the board to an X or or O based on player.
bool Board::makeMove(int x, int y, int playerTurn){

   if(board[x][y] == '.'){
      if(playerTurn == 1){
         cout << "PLAYER 1 HAS MADE HIS MOVE" << endl;
         board[x][y] = 'X';
         return true;
      }
      else if(playerTurn == 2){
         cout << "PLAYER 2 HAS MADE HIS MOVE." <<endl;
         board[x][y] = 'O';
         return true;
      }
   }

   else{
      return false;
   }
}

//nested for loop to print out the tic tac toe board
void Board::print(){
   cout << "CURRENT BOARD STATUS" << endl;
   cout << ' ';
   for (int i = 0; i < 3; i++)
      cout << ' ' << i;
   cout << endl;
   for (int i = 0; i < 3; i++){
      cout << i;
      for (int q = 0; q < 3; ++q){
         if(board[i][q] == ' ')
            board[i][q] = '.';
         cout << " " <<board[i][q];
      }
      cout << endl;
   }
}

//chekcs the board state and returns who has won based on the what is currently on the board
gameStatus Board::gameState(){
   int count = 0;

   if (board[0][0] == board[0][1] && board[0][0] == board[0][2]){
     if (board[0][0] == 'X')
         return currentStatus = x_has_won;
     else if (board[0][0] == 'O')
         return currentStatus = o_has_won;
   }

   if (board[0][0] == board[1][0] && board[0][0] == board[2][0]){
     if (board[0][0] == 'X')
         return currentStatus = x_has_won;
     else if (board[0][0] == 'O')
         return currentStatus = o_has_won;
   }

   if (board[0][0] == board[1][1] && board[0][0] == board[2][2]){
     if (board[0][0] == 'X')
         return currentStatus = x_has_won;
     else if (board[0][0] == 'O')
         return currentStatus = o_has_won;
   }

   if (board[0][1] == board[1][1] && board[0][1] == board[2][1]){
      if (board[0][1] == 'X')
         return currentStatus = x_has_won;
      else if (board[0][1] == 'O')
         return currentStatus = o_has_won;
   }

   if (board[0][2] == board[1][2] && board[0][2] == board[2][2]){
     if (board[0][2] == 'X')
         return currentStatus = x_has_won;
     else if (board[0][2] == 'O')
         return currentStatus = o_has_won;
   }

   if (board[1][0] == board[1][1] && board[1][0] == board[1][2]){
     if (board[1][0] == 'X')
         return currentStatus = x_has_won;
     else if (board[1][0] == 'O')
         return currentStatus = o_has_won;
   }

   if (board[2][0] == board[2][1] && board[2][0] == board[2][2]){
     if (board[2][0] == 'X')
      return currentStatus = x_has_won;
     else if (board[2][0] == 'O')
      return currentStatus = o_has_won;
   }

   if (board[2][0] == board[1][1] && board[2][0] == board[0][2]){
     if (board[2][0] == 'X')
         return currentStatus = x_has_won;
     else if (board[2][0] == 'O')
         return currentStatus = o_has_won;
   }

   for (int x = 0; x < 3; x++){
     for (int y = 0; y < 3; y++){
         if (board[x][y] == 'X' || board[x][y] == 'O')
           count++;
         else
           return currentStatus = game_is_still_in_process;
      }
   }

 return currentStatus = game_is_a_draw;
}

/* Check to see if this compiles
int main() {
   Board test;
   test.makeMove(1,2,1);
   test.print();

   return 0;
}
*/
