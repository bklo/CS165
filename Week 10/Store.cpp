/*********************************************************************
** Author: Brandon Lo
** Date:12/2/2016
** Description: store file where store is simulated checkout and checks for membership
*********************************************************************/
#include "Product.hpp"
#include "Store.hpp"
#include <iostream>
#include <string>

//for the set precision and fixed
#include <iomanip>
#include <algorithm>
using namespace std;

//adds a product to the vector inventory found in product class
void Store::addProduct(Product* p){
   inventory.push_back(p);
}

//adds a member to the members inventory found in Customer class
void Store::addMember(Customer* c){
   members.push_back(c);
}

//for loop to check for ID and returns the product object if found
Product* Store::getProductFromID(string productString){
   for (int i = 0; i < inventory.size(); i++){
         if (productString == inventory[i]->getIdCode()){
            return inventory[i];
         }
   }
   return NULL;
}

//for loop to check for ID and returns the member object if found
Customer* Store::getMemberFromID(string memberID){
   for (int i = 0; i < members.size(); i++){
         if (memberID == members[i]->getAccountID()){
            return members[i];
         }
   }
   return NULL;
}

//insert a string and it is capitalized and checked against the description and the titles of products
//made the whole search case insensitive. If it wants to be changed to first letter, the for loop can easily edited for upperCaseString
void Store::productSearch(string str){
   bool check = false;
   string upperCaseString, upperTitle, upperDescription;

   for (int c = 0; c < str.size(); c++)
      upperCaseString[c] = toupper(str[c]);

   for (int i = 0; i < inventory.size(); i++){
      string title = inventory[i]->getTitle();
      string description = inventory[i]->getDescription();

      for (int x = 0; x < title.size(); x++)
         upperTitle[x] = toupper(title[x]);

      for (int y = 0; y < description.size(); y++)
         upperDescription[y] = toupper(description[y]);

      //npos checks for occurence of -1. If -1, then it is not found.
      if (upperTitle.find(upperCaseString) != string::npos || upperDescription.find(upperCaseString) != string::npos){
         check = true;
         cout << inventory[i]->getTitle() << endl;
         cout << "ID code: " << inventory[i]->getIdCode() << endl;
         cout << "Price: " << inventory[i]->getPrice() << endl;
         cout << inventory[i]->getDescription() << endl;
      }

   }

   if (!check){
      cout << "Product not found" << endl;
   }
}

//adds a product to the cart of a member based on if the member ID was found, if the cart is empty or not, and if the product was found
void Store::addProductToMemberCart(string pID, string mID){
   bool membershipCheck = false;
   bool productCheck = false;
   bool quantityCheck = false;
   for (int i = 0; i < members.size(); i++){
      if (members[i]->getAccountID() == mID){
         membershipCheck = true;
         for (int q = 0; q < inventory.size(); q++){
            if(inventory[q]->getIdCode() == pID){
               productCheck = true;
               if(inventory[q]->getQuantityAvailable() > 0){
                  quantityCheck = true;
                  members[i]->addProductToCart(pID);
               }
            }
         }
      }
   }
   if (membershipCheck == false){
         cout << "Member #" << mID << " not found." << endl;
      }
   else if (productCheck == false){
      cout << "Product #" << pID << " not found" << endl;
   }
   else if (quantityCheck == false){
      cout << "Product is currently out of stock" << endl;
   }
}

//checks out the member based on what is in his cart and calculated what is in his cart based on whether he is a premium member or not
void Store::checkOutMember(string mID){
   vector<string> checkoutCart;
   Product* itemFromCart;
   double sum = 0.0;
   double shippingCost;
   bool membershipCheck = false;
   bool emptyCartCheck = false;
   int membershipNumber;

   for (int i = 0; i < members.size(); i++){
      if (mID == members[i]->getAccountID()){
         membershipCheck = true;
         membershipNumber = i;
      }
   }

   if(membershipCheck == false){
      cout << "Member #" << mID << " not found." << endl;
   }

   else if (membershipCheck == true){
      checkoutCart = members[membershipNumber]->getCart();

      for(int q = 0; q < checkoutCart.size(); q++){
         itemFromCart = getProductFromID(checkoutCart[q]);
         if (itemFromCart->getQuantityAvailable() > 0){

            cout << itemFromCart->getTitle() << " - $" << itemFromCart->getPrice() << endl;
            sum += itemFromCart->getPrice();
            itemFromCart->decreaseQuantity();
         }
         else{
            cout << "Product #" << itemFromCart->getIdCode() << ", " << itemFromCart->getTitle() << " is out of stock." << endl;
         }
      }
      if(checkoutCart.size() == 0){
         emptyCartCheck = true;
         cout << "There are no items in the cart" << endl;
      }

      if(members[membershipNumber]->isPremiumMember() == true && emptyCartCheck == false){
         cout << "Subtotal: $" << setprecision(2) << fixed << sum << endl;
         cout << "Shipping Cost: $0" << endl;
         cout << "Total: $"<< sum << endl;
         if(!checkoutCart.empty() == true)
            members[membershipNumber]->emptyCart();
      }

      else if (members[membershipNumber]->isPremiumMember() == false && emptyCartCheck == false){
         cout << "Subtotal: $" << setprecision(2) << fixed << sum << endl;
         shippingCost = sum * 0.07;
         cout << "Shipping Cost: $" << shippingCost<< endl;
         sum = sum + shippingCost;
         cout << "Total: $"<< sum << endl;
         if(checkoutCart.empty() == true)
            members[membershipNumber]->emptyCart();
      }
   }

}
