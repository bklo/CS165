/*********************************************************************
** Author: Brandon Lo
** Date:12/2/2016
** Description: TicTacToe file which has the function for the board moves and the tictactoe game and the printing of the game status and the printing of the current gameboard
*********************************************************************/
#include <iostream>
#include <string>
#include <cstring> //needed for toupper
#include "TicTacToe.hpp"
using namespace std;

// default constructor that sets the current player turncount and the option variable which is used to determine if player 1 is an X or O. (both to 1 as default)
TicTacToe::TicTacToe(){
   option = 1;
   playerTurnCount = 0;
}

//constructor that sets the option based on player input. Makes the player input uppercase
TicTacToe::TicTacToe(char playerInput){
   player = playerInput;
   player = toupper(player);
   if(player == 'X'){
      p1 = 'X';
      p2 = 'O';
      XStatus = 1;
      OStatus = 2;
      option = 1;
   }

   else if(player == 'O'){
      p1 = 'O';
      p2 = 'X';
      OStatus = 2;
      XStatus = 1;
      option = 2;
   }
   cout << "PLAYER 1 IS: " << p1 << endl;
   cout << "PLAYER 2 IS: " << p2 << endl;
}

//Returns which player's current turn
int TicTacToe::turnCounter(){
   if (option == 1){
      if (playerTurnCount %2 == 0)
         return XStatus;
      else
         return OStatus;
      }
   else{
      if (playerTurnCount %2 == 0)
         return OStatus;
      else
         return XStatus;
   }
}

//While loop which plays the game and tell the player's move. Checks the game status as it goes on. Has error bounds checking and if it is a digit or not.
void TicTacToe::play(){
   unsigned int x, y;
   player = turnCounter();

   if (player == 1)
      cout << "\nPLAYER 1'S TURN." << endl;

   if (player == 2)
         cout << "\nPLAYER 2'S TURN." << endl;

   currentBoard.print();
   cout << "PLEASE ENTER YOUR MOVE: ";
   cin >> x >> y;

   if ( x >= 3 || x < 0 || y >= 3 || y < 0){
      cout << "PLEASE SELECT AN VALID SPACE. TRY AGAIN:" <<endl;
      cin >> x >> y;
   }


   while(currentBoard.makeMove(x,y,player) == false){
         cout << "THAT SQUARE IS ALREADY TAKEN. TRY AGAIN: " << endl;
         cin.clear();
         cin >> x >> y;
   }

   playerTurnCount++;
   if(currentBoard.gameState() == game_is_still_in_process){
      play();
   }

   else if(currentBoard.gameState() == x_has_won){
        currentBoard.print();
        cout << "X HAS WON THE GAME." << endl;
   }

   else if(currentBoard.gameState() == o_has_won){
      currentBoard.print();
      cout << "O HAS WON THE GAME." << endl;
   }

   else if (currentBoard.gameState() == game_is_a_draw){
      currentBoard.print();
      cout << "THE GAME HAS ENDED IN A DRAW." << endl;
   }
}

//main function which starts the game and provides instructions at the beginning
int main(){
  cout << "Please enter a coordinate and type the x-coordinate first (left column) and y-coordinate second (top row). Please seperate the coordinates with a space. Have fun!" << endl;
  char start;
  cout << "\nWHAT WILL PLAYER 1 START WITH? X OR O?: ";
  cin >> start;
  cout << endl;
  TicTacToe game(start);
  game.play();
  return 0;
}
