/*********************************************************************
** Author: Brandon Lo
** Date:12/2/2016
** Description: header file for Board class specification file
*********************************************************************/
#ifndef BOARD_HPP
#define BOARD_HPP

enum gameStatus{
   x_has_won,
   o_has_won,
   game_is_a_draw,
   game_is_still_in_process
};

class Board{
   private:
      gameStatus currentStatus;
      char board[3][3];
      int x, y, playerTurn;
   public:
      Board();
      bool makeMove(int, int, int);
      gameStatus gameState();
      void print();
};

#endif
