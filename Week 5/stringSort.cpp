/*********************************************************************
** Author: Brandon Lo
** Date:10/26/2016
** Description:alphebetical selection sort from Chapter 9. Sort from a-z (cap insensitive) and also sorts by size (being > beings)
*********************************************************************/
#include <iostream>
#include <string>
using namespace std;

//compares the two words. Has a while loop while it iterates through the word, it checks. If it there is a difference, check becomes false and exits loop.
//else it just continues through the loop. If there is no difference in the word, it returns false automatically. Also has a check for similar words with same characters, being vs beings
bool asciiCompare(string Word1, string Word2){
   int ascii1 = 0, ascii2 = 0, count = 0;
   int total1 =0; int total2 =0;
   bool comparer = false, check = true, stringSimilar = false;

   for (int i = 0; i < (Word1.length() - 1);i++)
	total1 += toupper(Word1.at(i));

   for (int i = 0; i < (Word2.length() - 1);i++)
	total2 += toupper(Word2.at(i));

   //check to exit loop and comparer to tell whether it needs to swap or not
   while (check && (count < Word1.length()) && (count < Word2.length())){
      ascii1 = toupper(Word1.at(count));
      ascii2 = toupper(Word2.at(count));
      if (ascii1 < ascii2){
         comparer = true;
         check = false;
         }
      else if(ascii2 < ascii1){
         comparer = false;
         check = false;
      }

   //chekcs if majority of string is same, checks length of string if longer  aka being vs beings have different ascii totals
   else if( ascii1 == ascii2){
      if(Word1.length() < Word2.length())
	 	   if(total1 < total2)
			comparer = true;


      else if(Word2.length() < Word1.length())
   		if (total2 < total1)
   			comparer = true;
	}

      count++;
	}
   return comparer;
}


//sort strings using selection sort.
void selectionSort(string array[], int size){
   int startScan, minIndex;
   string minString;


   for (startScan = 0; startScan < (size - 1);startScan++){
      minIndex = startScan;
      minString = array[startScan];
      for(int index = startScan + 1; index < size; index++){
         if (asciiCompare(array[index],minString)){
            minString = array[index];
            minIndex = index;
         }
      }
      array[minIndex] = array[startScan];
      array[startScan] = minString;
   }
}

//book function to show array. Used just to display array easily.
void showArray(const string array[], int size){
   for (int count = 0; count < size; count++)
      cout << array[count] << " ";
   cout << endl;
}

/*
int main(){
   const int SIZE = 17;

   string values[SIZE] = {"bee", "Apple","apple","LetterKenny","money","Durrhurrdurr", "aa", "AA", "ab", "aB","bes","beis", "bei","be","ba","be","b"};

   cout << "The unsorted values are \n";
   showArray(values,SIZE);

   selectionSort(values,SIZE);

   cout << "The sorted values are \n";
   showArray(values,SIZE);

   return 0;
}
*/
