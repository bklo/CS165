/*********************************************************************
** Author: Brandon Lo
** Date:10/26/2016
** Description: box sort function to sort by volume
*********************************************************************/
#include "Box.hpp"

//uses bubble sort from chapter 9 in order to sort boxes from greatest volume to least volume
void boxSort(Box array[], int sizeIn){
   Box temp;
   bool swap;

   do{
   swap = false;
   for (int count = 0; count < (sizeIn -1); count++){
     if (array[count].getVolume() < array[count+1].getVolume()){
         temp = array[count];
         array[count] = array[count+1];
         array[count+1] = temp;
         swap = true;
       }
   }
   }while(swap);
   
}
