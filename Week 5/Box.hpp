/*********************************************************************
** Author: Brandon Lo
** Date:10/26/2016
** Description: box class source file that creates a class box that sets the length, width, height and has member functions that can get the surface area and volume.
*********************************************************************/
#ifndef BOX_HPP
#define BOX_HPP

//declaring class Box
class Box {
   private:
      double height,
      width,
      length;

   public:
      Box();
      Box(double, double, double);
      void setHeight(double);
      void setWidth(double);
      void setLength(double);
      double getVolume();
      double getSurfaceArea();
};

#endif
