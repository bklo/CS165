#include <iostream>
#include "Box.hpp"

void boxSort(Box array[], int size);

int main(){
  const int NUM_BOXES= 5;

  Box array[NUM_BOXES];

  array[0].setLength(25);
  array[0].setWidth(25);
  array[0].setHeight(25);

  array[1].setLength(5.5);
  array[1].setWidth(5.5);
  array[1].setHeight(5);

  array[2].setLength(18.5);
  array[2].setWidth(15);
  array[2].setHeight(15);

  array[3].setLength(250);
  array[3].setWidth(250);
  array[3].setHeight(250);

  array[4].setLength(205);
  array[4].setWidth(205);
  array[4].setHeight(205);

  boxSort(array, NUM_BOXES);

   for (int i = 0; i < NUM_BOXES; i++){
      std::cout << array[i].getVolume() << std::endl;
   }

  return 0;
}
