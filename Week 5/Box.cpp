/*********************************************************************
** Author: Brandon Lo
** Date:10/26/2016
** Description: box class source file that creates a class box that sets the length, width, height and has member functions that can get the surface area and volume.
*********************************************************************/
#include <iostream>
#include "Box.hpp"

//default constructor initializes default to 1
Box::Box(){
   setLength(1);
   setWidth(1);
   setHeight(1);
}

//constructor with 3 arguments to set the length, width, and height
Box::Box(double inputLength, double inputWidth, double inputHeight){
   setLength(inputLength);
   setWidth(inputWidth);
   setHeight(inputHeight);
}

//sets the input to the private variable
void Box::setLength(double inputLength){
   length = inputLength;
}

//sets the input to the private variable
void Box::setWidth(double inputWidth){
   width = inputWidth;
}

//sets the input to the private variable
void Box::setHeight(double inputHeight){
   height = inputHeight;
}

//calculates and returns the volume of the objects using the formula volume = l x w x h
double Box::getVolume(){
   return length * width * height;
}

//calculates and returns the volume of the objects using the formula SA = 2ab + 2bc + 2ac
double Box::getSurfaceArea(){
   return 2 * length * width + 2 * width * height + 2 * length * height;
}
