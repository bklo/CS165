/*********************************************************************
** Author: Brandon Lo
** Date:10/12/2016
** Description: Program that finds standard deviation from an array of pointers
*********************************************************************/
#include<iostream>
#include"Student.hpp"
#include<string>
#include<cmath>
using namespace std;

//takes array of pointers and size to return standard deviation
double stdDev(Student* array[], int size){
	double total = 0,
	acc = 0,
	avg = 0,
	standardDeviation = 0;

	//accumulator the average
	for (int count = 0; count < size; count++){
		total = total + array[count]->getScore();
	}
	avg = (total/size);

	//accumulator for sigma
	for (int count = 0; count < size; count++){
		acc = acc + ((array[count]->getScore() - avg)*((array[count]->getScore() - avg)));
	}

	standardDeviation = sqrt(acc/size);
	return standardDeviation;
}



/*
int main(){

	const int arraySize = 5;
	double standardDeviation= 0;


	Student array[arraySize] = {Student("Jordan", 91.2), Student("Brandon", 24.5), Student("Jeff", 32.59), Student("Phil", 77.09), Student("Archer", 35.50)};

	Student* Students[arraySize];

	for (int i=0; i < arraySize ; i++){
	   Students[i] = &array[i];
	}

	standardDeviation= stdDev(Students, arraySize);
	cout << "SD Should equal 26.725" <<endl;
	cout << standardDeviation<< endl;

	return 0;
}
/*
