/*********************************************************************
** Author: Brandon Lo
** Date:10/12/2016
** Description: Function to find mode of an integer array
*********************************************************************/
#include <vector>
#include <algorithm>
//for main function
#include <iostream>
#include <fstream>

//function findMode which takes an array and size and returns a vector containing the modes
std::vector<int> findMode(int arrayIn[], int arraySize){
   int* frequency,
   maxFreq=0;
   std::vector<int> vectReturn;
   frequency = new int[arraySize]();

   //Iterates through the array and increases the frequency. Second array used to check to stop duplicate maxes
   for (int count1 = 0; count1 < arraySize; count1++){
      for (int count2 = count1; count2 < arraySize; count2++){
   	   if (arrayIn[count1] == arrayIn[count2]){
   	      frequency[count1] += 1;
   	   }
   	}
   }

   // finds the maximum value in an array of integers
   for (int count = 0; count < arraySize; count++){
      if ( frequency[count] > maxFreq){
          maxFreq = frequency[count];
      }
   }

   //If a max number appears multiple number of times, then it reduces it by 1.
   for (int count = 0; count < arraySize; count++){
      if ( frequency[count] == maxFreq){
   	   vectReturn.push_back(arrayIn[count]);
         }
   }

   delete [] frequency;
   frequency = 0;
   std::sort(vectReturn.begin(), vectReturn.end());
   return vectReturn;
}




/*
std::vector<int> findMode(int[], int);
void displayVector(std::vector<int>);

int main() {
   std::ifstream inputFile;
   int arrayTest[100];
   std::vector<int> vect1;
   int count = 0;

   inputFile.open("numbers.txt");

   while (inputFile >> arrayTest[count]){
      count++;
   }

   vect1 = findMode(arrayTest, count);
   displayVector(vect1);
   inputFile.close();

   return 0;
   }

   void displayVector(std::vector<int> vectIn){
   for (int ii=0; ii < vectIn.size(); ii++){
      std::cout << vectIn[ii] << std::endl;
   }
}
*/
