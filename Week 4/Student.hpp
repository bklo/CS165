/*********************************************************************
** Author: Brandon Lo
** Date:10/12/2016
** Description: header file for Student class specification file
*********************************************************************/
#include<string>
using namespace std;

#ifndef STUDENT_HPP
#define STUDENT_HPP

//declaring class Student
class Student {
   private:
      double grade;
      string name;

   public:
      Student(string, double);
      double getScore();
      string getName();
};

#endif
