/*********************************************************************
** Author: Brandon Lo
** Date:10/12/2016
** Description: Class implementation of Students
*********************************************************************/
#include<iostream>
#include"Student.hpp"
#include<string>
#include<cmath>
using namespace std;

//constructor for Student. Takes 2 values and sets to 2 private variables
Student::Student(string nameInput, double gradeInput){
	name = nameInput;
	grade = gradeInput;
}

//returns Name once called
string Student::getName(){
	return name;
}

//returns grade once called
double Student::getScore(){
	return grade;
}
