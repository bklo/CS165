/*********************************************************************
** Author: Brandon Lo
** Date:10/12/2016
** Description: Program that orders 3 numbers without an array (modied with pointers)
*********************************************************************/
#include <iostream>
using namespace std;

//swap function that swaps the position of a and b
void swap(int *a, int *b){
   int temp = *b;
   *b = *a;
   *a = temp;
}

//if there are duplicates, then it reorders based on duplicates
int dupcheck(int *a, int *b, int *c) {
	if (*a == *b && *a > *c)// if c > a, then no need to change
		swap(c, a);
	else if (*a == *c && *c < *b)
		swap(b, c);
	else if (*a == *c && *c > *b)
		swap(a, b);
	else if (b == c && c < a) // if c > a, then no need to change
		swap(a, c);
	return *a, *b, *c;
}

//sort function that has 2 while loops to check positions of a and b.
void smallSort2 (int *a, int *b, int *c){
	if (*a == *b || *a == *c || *b == *c) {
		dupcheck(a, b, c);
	}
	else {
		while (*a > *c || *a > *b) { //if a is the larger than either b or c
			if (*a > *b) //if a is larger than b
				swap(a, b);
			else if (*a > *c) //if a is only larger than c
				swap(a, c);
		}
		while (*b > *c) { //checks if the middle is the largest
			if (*b > *a)
				swap(b, c);
			else
				swap(b, c);
		}
	}
}





/*
int main() {
	//correct order
   int a = 1, b = 2 , c = 3;
   smallSort2(&a,&b,&c);
   cout << a <<", " << b << ", " << c << endl;

   //b is largest
   a = 11, b = 3123, c = 2;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   //a is largest
   a = 435, b = 23, c = 32;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   //b and c are the same
   a = 3123, b = 3, c = 3;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   //b and c are the same
   a = 1, b = 3, c = 3;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   //a and b are the same
   a = 3, b = 3, c = 3123;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   //a and b are the same
   a = 3, b = 3, c = 1;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   //a and c are the same
   a = 3, b = 3123, c = 3;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   //a and c are the same
   a = 3, b = 1, c = 3;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   //a and b are the same
   a = 3, b = 3, c = 3;
   smallSort2(&a,&b,&c);
   cout << a << ", " << b << ", " << c << endl;

   return 0;
}
*/
