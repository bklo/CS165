/*********************************************************************
** Author: Brandon Lo
** Date:11/2/2016
** Description: recursive function to convert binary to Decimal and decimal to Binary
*********************************************************************/
#include <iostream>
#include <string>
#include <cmath>
using namespace std;

//takes in a string and returns an integer. Reads first element of string and then reading the number and then removing the first character of the string
int binToDec(string binIn){
   int length = binIn.length();

   if(length == 0)
      return 0;

   if(binIn.at(0) == '1')
      return pow(2,length-1) + binToDec(binIn.erase(0,1));

   else
      return binToDec(binIn.erase(0,1));

}

//takes in an int and converts to binary as a string.
string decToBin(unsigned int decIn){

   if (decIn == 0)
      return "";

   if (decIn%2 == 1)
      return decToBin(decIn/2) + "1";

   else
      return decToBin(decIn/2) + "0";
}
