/*********************************************************************
** Author: Brandon Lo
** Date:11/2/2016
** Description: recursive function to sum up an array of numbers
*********************************************************************/

#include <iostream>
using namespace std;

//uses recursion to set sum up the array. Sets sum to the first number in the array.
double summer(double array[], int SIZE){

   if (SIZE > 0){
      array[SIZE - 1] += array[SIZE];
      summer(array, (SIZE - 1));
   }

   return array[0];
}

/*
//book function from StringSort to show Array.
void showArray(const double array[], int size){
   for (int count = 0; count < size; count++)
      cout << array[count] << " ";
   cout << endl;
}

int main() {
   const int SIZE = 10;

   double array[SIZE] = {1,2,3,4,5,6,7,8,9,10};

   double total = summer(array, SIZE);

   cout << total << endl;

   showArray(array, SIZE);


   return 0;
}
*/
