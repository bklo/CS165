/*********************************************************************
** Author: Brandon Lo
** Date:11/9/2016
** Description: function that sets the width and length and can call functions area and perimeter to return a double value
*********************************************************************/
#include <iostream>
#include <cmath>
#include "Rectangle.hpp"
using namespace std;

//Constructor that sets the length and width
Rectangle::Rectangle(double lengthIn, double widthIn){
   setLength(lengthIn);
   setWidth(widthIn);
}

//Sets the public widthIn to the private variable width
void Rectangle::setWidth(double widthIn){
   width = widthIn;
}

//Sets the public lengthIn to the private variable length
void Rectangle::setLength(double lengthIn){
   length = lengthIn;
}

//returns pertimeter of a Rectangle with the function 2* (l + w)
double Rectangle::perimeter(){
   return width * 2 + length * 2;
}

//returns area of a Rectangle with the function (l * w)
double Rectangle::area(){
   return width * length;
}
