/*********************************************************************
** Author: Brandon Lo
** Date:11/9/2016
** Description: header file for Rectangle class specification file
*********************************************************************/
#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

class Rectangle{
   private:
      double length,
      width;
   public:
      void setWidth(double);
      void setLength(double);
      Rectangle(double, double);
      double perimeter();
      double area();
};

#endif
