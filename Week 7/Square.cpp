/*********************************************************************
** Author: Brandon Lo
** Date:11/9/2016
** Description: Square file that utalizes inheritence from the public rectangle class to set the length and width for squares
*********************************************************************/
#include "Rectangle.hpp"
#include "Square.hpp"

//Constructor that uses inheritence to set the length
Square::Square(double lengthIn): Rectangle(lengthIn,lengthIn){}

//Sets the length of a square
void Square::setLength(double lengthIn){
   Rectangle::setLength(lengthIn);
   Rectangle::setWidth(lengthIn);
}

//Sets the width of a square
void Square::setWidth(double lengthIn){
   Rectangle::setLength(lengthIn);
   Rectangle::setWidth(lengthIn);
}
