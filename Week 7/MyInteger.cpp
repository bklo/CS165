/*********************************************************************
** Author: Brandon Lo
** Date:11/9/2016
** Description: function that utalizes a pointer to allocate it to memory and uses a copy constructor and operator overloading to create object copies
*********************************************************************/
#include "MyInteger.hpp"
#include <iostream>
using namespace std;

//Constructor that takes an int and then dynamically allocates memory for an int and assigns value
MyInteger::MyInteger(int intInput){
  pInteger = new int;
  *pInteger = intInput;
}

// Copy constructor that setting pointer value to object passed to another copy.
MyInteger::MyInteger(const MyInteger& object){
  pInteger = new int;
  *pInteger = *(object.pInteger);
}

//deconstructor for MyInteger that deallocates memory
MyInteger::~MyInteger(){
  delete pInteger;
}

//Overloads the = operator so that it can set the object to the pointer
MyInteger MyInteger::operator=(const MyInteger& object){
  pInteger = new int;
  *pInteger = *(object.pInteger);
  return *this;
}

//Returns the pointer
int MyInteger::getMyInt(){
  return *pInteger;
}

//sets the pointer to intInput
void MyInteger::setMyInt(int intInput){
  *pInteger = intInput;
}

/*
int  main() {
   MyInteger obj1(17);
   MyInteger obj2 = obj1;
   std::cout << obj1.getMyInt() << std::endl;
   std::cout << obj2.getMyInt() << std::endl;

   obj2.setMyInt(9);
   std::cout << obj1.getMyInt() << std::endl;
   std::cout << obj2.getMyInt() << std::endl;

   MyInteger obj3(42);
   obj2 = obj3;
   std::cout << obj2.getMyInt() << std::endl;
   std::cout << obj3.getMyInt() << std::endl;

   obj3.setMyInt(1);
   std::cout << obj2.getMyInt() << std::endl;
   std::cout << obj3.getMyInt() << std::endl;
   return 0;
}
*/
