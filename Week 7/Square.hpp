/*********************************************************************
** Author: Brandon Lo
** Date:11/9/2016
** Description: header file for Square class specification file
*********************************************************************/
#include "Rectangle.hpp"

#ifndef SQUARE_HPP
#define SQUARE_HPP

//Sets Rectangle class as public so it can be inherited
class Square : public Rectangle{
   public:
      Square(double);
      void setLength(double);
      void setWidth(double);
};

#endif
