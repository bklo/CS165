/*********************************************************************
** Author: Brandon Lo
** Date:11/23/2016
** Description: Implementation of postfixEval that implements a stack
*********************************************************************/
#include <string.h>
#include <stack>
#include <cstdlib>
#include <iostream>
using namespace std;

//implements postfix evaulation method with a double stack and returns a double answer depending on the postfix expression
double postfixEval(char inputString[]){
   stack<double> doubleStack;
   double temp1, temp2;
   char *charPointer;

   //strtok is used to break up the string into a smaller string
   charPointer = strtok(inputString, " ");

   //since postfix evaluation is in the form of 23*,then the first 2 elements of charPointer is pushed into the stack and then evaulated
   while (charPointer != NULL){

      if (*charPointer == '+'){
   	  temp1 = doubleStack.top();
   	  doubleStack.pop();
   	  temp2 = doubleStack.top();
   	  doubleStack.pop();
   	  doubleStack.push(temp1 + temp2);
   	}

      else if (*charPointer == '-'){
   	  temp1 = doubleStack.top();
   	  doubleStack.pop();
   	  temp2 = doubleStack.top();
   	  doubleStack.pop();
   	  doubleStack.push(temp1 - temp2);
   	}

      else if (*charPointer == '*'){
   	  temp1 = doubleStack.top();
   	  doubleStack.pop();
   	  temp2 = doubleStack.top();
   	  doubleStack.pop();
   	  doubleStack.push(temp1 * temp2);
   	}

      else if (*charPointer == '/'){
   	  temp1 = doubleStack.top();
   	  doubleStack.pop();
   	  temp2 = doubleStack.top();
   	  doubleStack.pop();
   	  doubleStack.push(temp1 / temp2);
   	}

      else{
   	  doubleStack.push(atof(charPointer));
   	}

      charPointer = strtok(NULL, " ");
      }

  return doubleStack.top();
}
