/*********************************************************************
** Author: Brandon Lo
** Date:11/23/2016
** Description: header file for StringList class specification file
*********************************************************************/
#ifndef STRINGLIST_HPP
#define STRINGLIST_HPP
#include <string>
#include <vector>
using namespace std;


class StringList{
   private:

      //make a inherited struct ListNode because ListNode is commonly called
      struct ListNode{
         public:
            string stringHolder;
            ListNode* next;
            ListNode(string inputString, ListNode * currentPosition = NULL){
               stringHolder = inputString;
               next = currentPosition;
            }
      };
      ListNode* head;

   public:
      StringList();
      StringList(StringList&);
      ~StringList();
      void add(string);
      int positionOf(string);
      bool setNodeVal(int, string);
      vector<string> getAsVector();
};

#endif
