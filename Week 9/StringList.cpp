/*********************************************************************
** Author: Brandon Lo
** Date:11/23/2016
** Description: Implementation of StringList that creates a linked list which has functions to turn it into a vector, add, find position of and set node value.
*********************************************************************/
//included for unit testing
#include <iostream>
#include <string>
#include <vector>
#include "StringList.hpp"
using namespace std;

//default constructor
StringList::StringList(){
   head = NULL;
}

//copy constructor that utalizes the getAsVector to copy
StringList::StringList(StringList &object){
   vector<string> stringVector = object.getAsVector();
   head = NULL;
   int vecSize = stringVector.size();

   if(vecSize != 0){
      for (int i = 0; i < vecSize; i++){
         if(i == 0){
            head = new ListNode(stringVector[i]);
         }
         else{
            add(stringVector[i]);
         }
      }
   }

}

//deconstructor if the list isn't empty, then it iterates though and sets everything to NULL to prevent hanging pointers
StringList::~StringList(){
   ListNode* temp1 = head;
   ListNode* temp2 = NULL;

   if (temp1 != NULL){
      while (temp1->next != NULL){
         temp2 = temp1->next;
         delete temp1;
         temp1 = temp2;
      }
   }

   delete temp1;
   temp1 = NULL;
}

//Adds to the linked list with two conditionals depending if the list is empty or not
void StringList::add(string inputString){
   ListNode *current = head;

   if (current == NULL){
      head = new ListNode(inputString);
      return;
   }

   while(current->next != NULL){
      current = current->next;
   }

   current->next = new ListNode(inputString);
}

//Returns the positionOf a string, returns -1 if string is not in the list. Default is that the string is not in the linked list
int StringList::positionOf(string inputString){
   int currentPosition = 0;
   ListNode *current = head;

   //If list is empty return -1
   if(current == NULL){
      return -1;
   }

   //while loop to iterate through the linked list and has an accumulator on position (execute when string is not found)
   while ( ((current->stringHolder).compare(inputString) != 0) && (current->next != NULL)){
      current = current->next;
      currentPosition++;
   }

   //if the string is found it returns the position
   if(current->stringHolder == inputString){
      return currentPosition;
   }

   return -1;
}

//sets value of the node at the position of the value of the string parameter
bool StringList::setNodeVal(int inputInt, string inputString){
   ListNode * current = head;
   int currentPosition = 0;

   if (inputInt < 0){
      return false;
   }

   while(currentPosition != inputInt){
      if (current->next == NULL){
         return false;
      }
      current = current->next;
      currentPosition++;
   }

   current->stringHolder = inputString;
   return true;
}

//turns the linked list in to a vector and returns a string vector
vector<string> StringList::getAsVector(){
   vector<string> stringVector;
   ListNode* current = head;

   if (current == NULL){
      return stringVector;
   }

   stringVector.push_back(current->stringHolder);

   while(current->next != NULL){
      current = current->next;
      stringVector.push_back(current->stringHolder);
   }

   return stringVector;
}
